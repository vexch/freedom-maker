#
# This file is part of Freedom Maker.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Package containing all the builders.
"""

# pylint: disable=unused-import
from . import a20_olinuxino_lime2
from . import a20_olinuxino_lime
from . import a20_olinuxino_micro
from . import amd64
from . import arm
from . import banana_pro
from . import beaglebone
from . import cubieboard2
from . import cubietruck
from . import i386
from . import lamobo_r1
from . import pcduino3
from . import pine64_plus
from . import pine64_lts
from . import qemu_amd64
from . import qemu_i386
from . import raspberry_pi_2
from . import raspberry_pi_3
from . import raspberry_pi_3_b_plus
from . import raspberry_pi_with_uboot
from . import vagrant
from . import virtualbox_amd64
from . import virtualbox_i386
