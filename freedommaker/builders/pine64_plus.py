#
# This file is part of Freedom Maker.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Worker class to build Pine64+ image.
"""

from .a64 import A64ImageBuilder


class Pine64PlusImageBuilder(A64ImageBuilder):
    """Image builder for Pine64+ target."""
    machine = 'pine64-plus'
    flash_kernel_name = 'Pine64+'
    u_boot_target = 'pine64_plus'
